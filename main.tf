terraform {
  required_version = ">= 1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.68.0"
    }
  }

  backend "s3" {
    bucket      = "terraform-brunolabs"
    key         = "aws-ec2/terraform.tfstate"
    region      = "us-east-1"
    profile     = "terraform"
    acl         = "private"
    encrypt     = "true"
    max_retries = "5"
  }
}

provider "aws" {
  profile = var.aws_profile
  region  = "us-east-1"
}